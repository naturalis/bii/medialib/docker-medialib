<?php

if (!class_exists(ZipArchive::class)) {
  http_response_code(500);
  echo 'Zip NOT enabled!' . PHP_EOL;
  return;
}

$zip = new ZipArchive();
$result = $zip->open('zip_test.zip');
if ($result !== TRUE) {
  http_response_code(500);
  echo 'Zip failed to open zip_test.zip: ' . $result . PHP_EOL;
  return;
}

$result = $zip->extractTo('/tmp/zip_test');
$zip->close();

if ($result !== TRUE) {
  http_response_code(500);
  echo 'Zip failed to extract zip_test.zip: ' . $result . PHP_EOL;
  return;
}

$textFile = file_get_contents('//tmp/zip_test/test.txt');
if (!$textFile === 'Test!') {
  http_response_code(500);
  echo 'Text file is corrupt: ' . $textFile . PHP_EOL;
  return;
}

echo 'Zip works fine!' . PHP_EOL;
