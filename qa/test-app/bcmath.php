<?php

if (!function_exists('bcpowmod')) {
  http_response_code(500);
  echo 'BC Math NOT enabled!' . PHP_EOL;
  return;
}

echo 'BC Math works fine!' . PHP_EOL;
