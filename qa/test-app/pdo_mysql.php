<?php

if (!extension_loaded('pdo')) {
  http_response_code(500);
  echo 'PDO NOT enabled!' . PHP_EOL;
  return;
}

if (!extension_loaded('pdo_mysql')) {
  http_response_code(500);
  echo 'PDO MySQL NOT enabled!' . PHP_EOL;
  return;
}

echo 'PDO MySQL works fine!' . PHP_EOL;
