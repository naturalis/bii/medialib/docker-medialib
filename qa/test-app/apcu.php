<?php

if (!function_exists('apcu_enabled') || !apcu_enabled()) {
  http_response_code(500);
  echo 'APCU NOT enabled!' . PHP_EOL;
  return;
}

$value = $expectedValue = 'value';
apcu_store('test', $value);
$value = apcu_fetch('test');

if ($expectedValue !== $value) {
  http_response_code(500);
  echo 'APCU does NOT store or fetch values!' . PHP_EOL;
  return;
}

echo 'APCU works fine!' . PHP_EOL;
