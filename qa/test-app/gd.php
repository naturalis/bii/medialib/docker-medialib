<?php

if (!function_exists('gd_info')) {
  http_response_code(500);
  echo 'GD NOT enabled!' . PHP_EOL;
  return;
}

/** @noinspection ForgottenDebugOutputInspection */
var_export(gd_info());
