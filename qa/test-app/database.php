<?php

if (!function_exists('pdo_drivers') || !in_array('mysql', pdo_drivers(), TRUE)) {
  http_response_code(500);
  echo 'PDO NOT enabled!' . PHP_EOL;
  return;
}

try {
  new PDO(
    'mysql:host=mariadb;dbname=develop',
    'develop',
    'develop'
  );
}
catch (PDOException $exception) {
  http_response_code(500);
  echo 'PDO connection failed: ' . $exception->getMessage() . PHP_EOL;
  return;
}

echo 'PDO works fine!' . PHP_EOL;
