#!/bin/sh
set -eu

if [ -z ${1+x} ]; then
  echo Missing name argument
  exit 1
fi
name=$1
image=$CI_REGISTRY_IMAGE/$name:build_$CI_COMMIT_REF_SLUG

docker pull "$image"

docker tag "$image" "$CI_REGISTRY_IMAGE/$name:$CI_PIPELINE_ID"
docker tag "$image" "$CI_REGISTRY_IMAGE/$name:$CI_COMMIT_REF_SLUG"

if [ "$CI_COMMIT_BRANCH" = "main" ]; then
  docker tag "$image" "$CI_REGISTRY_IMAGE/$name:latest"
  docker push "$CI_REGISTRY_IMAGE/$name:latest"
fi

docker push "$CI_REGISTRY_IMAGE/$name:$CI_PIPELINE_ID"
docker push "$CI_REGISTRY_IMAGE/$name:$CI_COMMIT_REF_SLUG"
